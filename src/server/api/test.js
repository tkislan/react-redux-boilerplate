import { Router } from 'express';

export default function() {
  const router = Router({ mergeParams: true });

  router.get('/data', (req, res) => res.json({ data: {} }));

  return router;
}
