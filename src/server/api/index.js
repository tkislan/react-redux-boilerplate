import { Router } from 'express';

import test from './test';

export default function(meta) {
  const router = Router({ mergeParams: true });

  router.use('/test', test(meta));

  router.use('*', (req, res) => {
    res.status(404).json({ error: 'API call doesn\'t exist' });
  });

  return router;
}
