import React from 'react';
import { IntlProvider } from 'react-intl';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';

import createStore from './store/config';

import App from './App_Redux';

const store = createStore();

const ws = new WebSocket(`ws://${window.location.host}/ws`);
ws.onopen = () => console.log('ws open');
ws.onmessage = (m) => {
  console.log('ws message:', m);
  const data = JSON.parse(m.data);
  console.log('ws message data:', data);
};
ws.onclose = () => console.log('ws close');

export default function() {
  return (
    <MuiThemeProvider>
      <Provider store={store}>
        <IntlProvider locale="en">
          <BrowserRouter>
            <Route path="/" component={App} />
          </BrowserRouter>
        </IntlProvider>
      </Provider>
    </MuiThemeProvider>
  );
}
