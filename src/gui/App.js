import React from 'react';

import { Switch, Route, Redirect } from 'react-router-dom';

import { rootStyle } from './App.scss';

export default class App extends React.PureComponent {
  render() {
    return (
      <div className={rootStyle}>
        <Switch>
          <Route exact path="/a" render={() => (<div>a</div>)} />
          <Route exact path="/b" render={() => (<div>b</div>)} />
        </Switch>
      </div>
    );
  }
}

