import { TEST_ACTION } from '../actions/test';

const initialState = {

};

export default function(state = initialState, action) {
  switch (action.type) {
    case TEST_ACTION:
      return state;
    default:
      return state;
  }
}
