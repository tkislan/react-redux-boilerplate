import { put, call, fork, delay, takeEvery } from 'redux-saga/effects';

import { TEST_ACTION } from '../actions/test';

export function* test() {
  try {
    yield put({ type: `${TEST_ACTION}_PENDING` });

    delay(3000);

    yield put({ type: `${TEST_ACTION}_OK`, data: {} });
  } catch (error) {
    yield put({ type: `${TEST_ACTION}_ERROR`, error: error.message });
  }
}

function* watchTestAction() {
  yield takeEvery(TEST_ACTION, test);
}

export default function* () {
  yield fork(watchTestAction);
}
