import { fork } from 'redux-saga/effects';

import docker from './docker';

export default function* root() {
  yield fork(docker);
}
